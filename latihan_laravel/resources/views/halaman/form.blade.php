@extends('layout.master')

@section('judul')
Halaman Form  
@endsection

@section('content')
    <h2>Buat Account Baru</h2>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First name :</label><br><br>
            <input type="text" name="firstname" placeholder="first name"><br><br>
        <label>Last name :</label><br><br>
            <input type="text" name="lastname" placeholder="last name"><br><br>
        <label>Gender</label><br><br>
            <input type="radio" name="gender">Male <br><br>
            <input type="radio" name="gender">Female <br><br>
        <label>Nationality</label><br><br>
            <select name="negara" id="">
                <option value="1">Indonesia</option>
                <option value="2">Amerika</option>
                <option value="3">Inggris</option>
            </select><br><br>
        <label>Language Spoken</label><br><br>
            <input type="checkbox" name="bahasa">Bahasa Indonesia <br>
            <input type="checkbox" name="bahasa">English <br>
            <input type="checkbox" name="bahasa">Other <br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10" placeholder="Masukan bio" ></textarea><br>
        <input type="submit" value="kirim">
    </form>
@endsection
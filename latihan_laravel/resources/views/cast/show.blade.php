@extends('layout.master')

@section('judul')
Halaman Detail Pemeran Film    
@endsection

@section('content')
    <a href="/cast" class="btn btn-info btn-sm mb-3">Back to list</a>
    <h2>{{ $cast->nama }}</h2>
    <p>Umur : {{ $cast->umur }}</p>
    <p>Bio  : {{ $cast->bio }}</p>
@endsection
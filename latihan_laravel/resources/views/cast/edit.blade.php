@extends('layout.master')

@section('judul')
Halaman Edit Pemeran Film    
@endsection

@section('content')

<form action="/cast/{{ $cast->id }}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{ $cast->nama }}" class="form-control" placeholder="masukan nama" >
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" value="{{ $cast->umur }}" class="form-control" placeholder="masukan umur" >
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" cols="30" rows="10" class="form-control" placeholder="masukan bio disini">{{ $cast->bio }}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>        
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/cast" class="btn btn-light btn-right">Cancel</a>
  </form>


@endsection
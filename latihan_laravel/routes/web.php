<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');
Route::get('/register', 'AuthController@form');
Route::post('/welcome', 'AuthController@kirim');
Route::get('/data-table', 'IndexController@table');

// CRUD cast
// Create
Route::get('/cast/create', 'CastController@create'); //masuk ke form tambah cast
Route::post('/cast', 'CastController@store'); //menyimpan data form ke database table cast

// Read
Route::get('/cast', 'CastController@index'); // tampilkan ke blade
Route::get('/cast/{cast_id}', 'CastController@show'); // route detail cast

// Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // mengarah ke edit
Route::put('/cast/{cast_id}', 'CastController@update');   // untuk update

// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); // menghapus database table cast
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
    return view('halaman.form', [
        "title" => "form"
        ]);
    }
    public function kirim(request $request){
        $firstname = $request['firstname'];
        $lastname  = $request['lastname'];
        return view('halaman.welcome', [
            "title" => "welcome"
        ], compact('firstname','lastname') ); 
    }
}
